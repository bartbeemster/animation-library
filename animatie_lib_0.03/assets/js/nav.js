function nav() {
    var mainNavItems = document.querySelectorAll('.mainNav > li');
    for (var i = 0; i < mainNavItems.length; i++) {
        mainNavItems[i].addEventListener('click', function() {
            var activeMainNav = document.querySelector('.activeMainNav');
            // if (activeMainNav !== null) {
            //     activeMainNav.classList.remove('activeMainNav');
            // }
            this.classList.toggle('activeMainNav');
            showSubNav(this.getAttribute('data-mainNav'));
        });
    }

    var subNavItems = document.querySelectorAll('.subNav > li');
    for (var i = 0; i < subNavItems.length; i++) {
        subNavItems[i].addEventListener('click', function() {
            var activeSubNav = document.querySelector('.activeSubNav');
            if (activeSubNav !== null) {
                activeSubNav.classList.remove('activeSubNav');
            }
            this.classList.add('activeSubNav');
        });
    }

    function showSubNav(animationCategory) {
        var currentActiveSubNavDropdown = document.querySelector('.activeSubNavDropdown');
        // if (currentActiveSubNavDropdown !== null) {
        //     currentActiveSubNavDropdown.classList.remove('activeSubNavDropdown');
        // }
        var subNavDropdown = document.querySelector('.subNav[data-mainNav="' + animationCategory + '"]');
        subNavDropdown.classList.toggle('activeSubNavDropdown');
    }
}