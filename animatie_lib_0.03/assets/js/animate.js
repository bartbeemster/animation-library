function animate() {
	var previewAnimationButton = document.querySelectorAll('.animationSpecs__button');
	for (var i = 0; i < previewAnimationButton.length; i++) {
		previewAnimationButton[i].addEventListener('click', function() {
			fireAnimation(this.getAttribute('data-animation'));
		});
	}

	function fireAnimation(animation) {
		var targetAnimation = document.querySelector('.animated[data-animation="' + animation + '"]');
		targetAnimation.classList.toggle(animation);
	}

	function clearActive(animation) {
		var targetAnimation = document.querySelector('.animated[data-animation="' + animation + '"]');
		targetAnimation.classList.remove(animation);
	}
}