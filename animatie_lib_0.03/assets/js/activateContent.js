function activateContent() {
	var subNavItem = document.querySelectorAll('.subNav > li');
	for (var i = 0; i < subNavItem.length; i++) {
		subNavItem[i].addEventListener('click', function() {
            showAnimationContent(this.getAttribute('data-contentLink'));
        });
	}

	function showAnimationContent(animationName) {
		var currentActiveContent = document.querySelector('.activeContent');
        if (currentActiveContent !== null) {
            currentActiveContent.classList.remove('activeContent'); 
        }
        
		var newActiveContent = document.querySelector('.content[data-contentLink="' + animationName + '"]');
		newActiveContent.classList.add('activeContent');
	}

	function activatePreviewButton(animationName) {
		// var currentActiveButton = document.querySelector('.activeButton');
		// if (currentActiveButton !== null) {
		// 	currentActiveButton.classList.remove('.active');
		// }
		var previewButton = document.querySelector('.animationSpecs__button[data-contentLink="' + animationName + '"]');
		previewButton.classList.add('activeButton');
	}
}