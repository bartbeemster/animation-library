function tweakAnimation() {
	function getAnimationValues(animationName) {
		var easing = document.querySelector('.easing[data-animation="' + animationName + '"]');
		var duration = document.querySelector('.duration[data-animation="' + animationName + '"]');

		if (duration.value == '') {
			duration.value = 0.5;
		} else if (duration.value < 0) {
			duration.value = 0.5;
		}
		setAnimationTweaks(animationName, duration.value, easing.value);
	}

	function setAnimationTweaks(animationName, duration, easing) {
		var animation = document.querySelector('.animated[data-animation="' + animationName + '"]');
		animation.style.transition = 'all ' + duration + 's ' + easing;
	}

	var previewAnimationButton = document.querySelectorAll('.animationSpecs__button');
	for (var i = 0; i < previewAnimationButton.length; i++) {
		previewAnimationButton[i].addEventListener('click', function() {
			getAnimationValues(this.getAttribute('data-animation'));
		});
	}
}

