var blocks = document.querySelectorAll('section');
for (var i = 0; i < blocks.length; i++) {
	blocks[i].addEventListener('click', function() {
		animate(this.getAttribute('data-animation'));
	});
}

function animate(target) {
	var animation = document.querySelector('.animated[data-animation="' + target + '"]');
	animation.classList.add(target);

	setTimeout(function() { 
		animation.classList.remove(target); 
	}, 1000);

}