function animate() {
	var blocks = document.querySelectorAll('.content>div');
	for (var i = 0; i < blocks.length; i++) {
		blocks[i].addEventListener('click', function() {
			fireAnimation(this.getAttribute('data-animation'));
		});
		blocks[i].addEventListener('mouseout', function() {
			clearActive(this.getAttribute('data-animation'));
		});
	}

	function fireAnimation(animation) {
		var targetAnimation = document.querySelector('.animated[data-animation="' + animation + '"]');
		targetAnimation.classList.toggle(animation);
	}

	function clearActive(animation) {
		var targetAnimation = document.querySelector('.animated[data-animation="' + animation + '"]');
		targetAnimation.classList.remove(animation);
	}
}