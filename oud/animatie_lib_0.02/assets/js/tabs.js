function tabs() {
   var navItem = document.querySelectorAll('li');
    for (var i = 0; i < navItem.length; i++) {
        navItem[i].addEventListener('click', function() {
            var activeNav = document.querySelector('.activeNav');
            if (activeNav !== null) {
                activeNav.classList.remove('activeNav');
            }
            this.classList.add('activeNav');
            showTabsContent(this.getAttribute('data-link'));
        });
    }

    function showTabsContent(target) {
        var currentActiveContent = document.querySelector('.activeContent');
        if (currentActiveContent !== null) {
            currentActiveContent.classList.remove('activeContent'); 
        }

        var newActiveContent = document.querySelector('.content[data-link="' + target + '"');
        newActiveContent.classList.add('activeContent');
    } 
}
